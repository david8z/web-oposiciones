from django.db import models

COMUNIDADES = (
    ( 'andalucia', 'Andalucía'),
    ( 'aragon', 'Aragón'),
    ( 'asturias', 'Principado de Asturias'),
    ( 'islas-baleares', 'Islas Baleares'),
    ( 'canarias', 'Canarias'),
    ( 'cantabria', 'Cantabria'),
    ( 'castilla-y-leon', 'Castilla y León'),
    ( 'castilla-la-mancha', 'Castilla La Mancha'),
    ( 'cataluña', 'Cataluña'),
    ( 'comunidad-valenciana', 'Comunidad Valenciana'),
    ( 'extremadura', 'Extremadura'),
    ( 'galicia', 'Galicia'),
    ( 'madrid', 'Comunidad de Madrid'),
    ( 'murcia', 'Región de Murcia'),
    ( 'navarra', 'Comunidad Foral de Navarra'),
    ( 'pais-vasco', 'País Vasco'),
    ( 'la-rioja', 'La Rioja'),
    ( 'ceuta', 'Ciudad Autónoma de Ceuta'),
    ( 'melilla', 'Ciudad Autónoma de Melilla')
)

class Comunidad(models.Model):
    """
    """
    nombre = models.CharField(primary_key=True, choices=COMUNIDADES, max_length=20)
    
    def __str__(self):
        return self.nombre
    
    class Meta:
        verbose_name_plural = 'Comunidades'
