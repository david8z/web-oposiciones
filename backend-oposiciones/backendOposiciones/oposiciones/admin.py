from django.contrib import admin
from .models import Oposicion, PuntuacionMeritoAcademico, AsignaturaEspecial, LicenciaturaGrado
from import_export.admin import ImportExportModelAdmin
from import_export import resources
from django.utils.html import mark_safe

class PuntuacionMeritoAcademicoResource(resources.ModelResource):
    class Meta:
        model = PuntuacionMeritoAcademico

class PuntuacionMeritoAcademicoModelAdmin(ImportExportModelAdmin):
    resource_class = PuntuacionMeritoAcademicoResource
    fieldsets = (
        ('GENERAL', {
            'fields': ('puntucacion_maxima',),
        }),
        ('EXPEDIENTE ACADÉMICO LICENCIATURA', {
            'fields': ('puntucacion_maxima_expediente','licenciaturas_grados_requeridos', ('puntos_por_asignatura_notable','puntos_por_asignatura_sobresaliente', 'puntos_por_asignatura_matricula'),'asignaturas_especiales'),
        }),
        ('TITULO EXTRA', {
            'fields': ('puntuacion_maxima_licenciaturas_grados_relacionados','licenciaturas_grados_relacionados', 'puntos_por_licenciaturas_grados_extra_sobresaliente', 'puntos_por_licenciaturas_grados_extra_notable','puntos_por_licenciaturas_grados_extra_aprobado'),
        }),
        ('MASTER', {
            'fields': ('puntuacion_maxima_master', 'puntos_por_master', ),
        }),
        ('DOCTORADO', {
            'fields': ('puntuacion_maxima_doctorado', ('puntos_por_doctorado','puntos_por_sobresaliente_doctorado', 'puntos_por_cumlaude_doctorado')),
        }),
        ('PREMIOS EXTRAORDINARIOS', {
            'fields': ('puntuacion_maxima_premio', ('puntos_por_premio_regional', 'puntos_por_premio_nacional'),),
        }),
        ('CURSOS', {
            'fields': ('puntuacion_maxima_cursos', ('requisito_administracion_publica','requisito_sociedades_cientificas', 'requisito_interes_cientifico_curso'), 'puntos_por_hora'),
        }),
        ('PONENCIAS', {
            'fields': ('puntuacion_maxima_ponencias', ('requisito_entidad_oficial', 'requisito_interes_cientifico'), ('puntos_por_ponencia', 'puntos_por_ponencia_primer_firmante')),
        }),
        ('PUBLICACIONES', {
            'fields': ('puntuacion_maxima_publicaciones', ('puntos_por_revista_no_sci', 'puntos_por_revista_no_sci_segundo_firm', 'puntos_por_revista_no_sci_primer_firm' ), ('puntos_por_revista_sci', 'puntos_por_revista_sci_segundo_firm', 'puntos_por_revista_sci_primer_firm'), ('requisito_ambito_nacional', 'requisito_ambito_internacional'), ('puntos_por_capitulo_primer_firm', 'puntos_por_capitulo'), ('puntos_por_libro_primer_autor', 'puntos_por_libro')),
        }),
        ('OTROS MERITOS ACADEMICOS', {
            'fields': ('puntuacion_maxima_otros_meritos', ),
        })
    )
    list_display = ['id', 'expediente_academico', 'titulo_extra', 'master', 'doctorado', 'premios_extraordinarios', 'cursos', 'ponencias', 'publicaciones', 'otros_meritos']

    def expediente_academico(self, obj):
        if obj.puntucacion_maxima_expediente  is not None:
            return mark_safe(
                    '<img src="/static/admin/img/icon-yes.svg" alt="True"><span>%s</span>' %  obj.puntucacion_maxima_expediente
                )
        else:
            return mark_safe(
                    '<img src="/static/admin/img/icon-no.svg" alt="False">'
                )
    
    def titulo_extra(self, obj):
        if obj.puntuacion_maxima_licenciaturas_grados_relacionados  is not None:
            return mark_safe(
                    '<img src="/static/admin/img/icon-yes.svg" alt="True"><span>%s</span>' % obj.puntuacion_maxima_licenciaturas_grados_relacionados
                )
        else:
            return mark_safe(
                    '<img src="/static/admin/img/icon-no.svg" alt="False">'
                )

    def master(self, obj):
        if obj.puntuacion_maxima_master  is not None:
            return mark_safe(
                    '<img src="/static/admin/img/icon-yes.svg" alt="True"><span>%s</span>' % obj.puntuacion_maxima_master
                )
        else:
            return mark_safe(
                    '<img src="/static/admin/img/icon-no.svg" alt="False">'
                )
    
    def doctorado(self, obj):
        if obj.puntuacion_maxima_doctorado  is not None:
            return mark_safe(
                    '<img src="/static/admin/img/icon-yes.svg" alt="True"><span>%s</span>' % obj.puntuacion_maxima_doctorado
                )
        else:
            return mark_safe(
                    '<img src="/static/admin/img/icon-no.svg" alt="False">'
                )

    def premios_extraordinarios(self, obj):
        if obj.puntuacion_maxima_premio  is not None:
            return mark_safe(
                    '<img src="/static/admin/img/icon-yes.svg" alt="True"><span>%s</span>' % obj.puntuacion_maxima_premio
                )
        else:
            return mark_safe(
                    '<img src="/static/admin/img/icon-no.svg" alt="False">'
                )
    
    def cursos(self, obj):
        if obj.puntuacion_maxima_cursos  is not None:
            return mark_safe(
                    '<img src="/static/admin/img/icon-yes.svg" alt="True"><span>%s</span>' % obj.puntuacion_maxima_cursos
                )
        else:
            return mark_safe(
                    '<img src="/static/admin/img/icon-no.svg" alt="False">'
                )

    def ponencias(self, obj):
        if obj.puntuacion_maxima_ponencias  is not None:
            return mark_safe(
                    '<img src="/static/admin/img/icon-yes.svg" alt="True"><span>%s</span>' % obj.puntuacion_maxima_ponencias
                )
        else:
            return mark_safe(
                    '<img src="/static/admin/img/icon-no.svg" alt="False">'
                )

    def publicaciones(self, obj):
        if obj.puntuacion_maxima_publicaciones  is not None:
            return mark_safe(
                    '<img src="/static/admin/img/icon-yes.svg" alt="True"><span>%s</span>' % obj.puntuacion_maxima_publicaciones
                )
        else:
            return mark_safe(
                    '<img src="/static/admin/img/icon-no.svg" alt="False">'
                )
        
    
    def otros_meritos(self, obj):
        if obj.puntuacion_maxima_otros_meritos  is not None:
            return mark_safe(
                    '<img src="/static/admin/img/icon-yes.svg" alt="True"><span>%s</span>' % obj.puntuacion_maxima_otros_meritos
                )
        else:
            return mark_safe(
                    '<img src="/static/admin/img/icon-no.svg" alt="False">'
                )

class LicenciaturaGradoResource(resources.ModelResource):
    class Meta:
        model = LicenciaturaGrado
        import_id_fields = ['slug']

class LicenciaturaGradoModelAdmin(ImportExportModelAdmin):
    resource_class = LicenciaturaGradoResource

class AsignaturaEspecialResource(resources.ModelResource):
    class Meta:
        model = AsignaturaEspecial
        import_id_fields = ['slug']

class AsignaturaEspecialModelAdmin(ImportExportModelAdmin):
    resource_class = AsignaturaEspecialResource

admin.site.register(Oposicion)
admin.site.register(PuntuacionMeritoAcademico, PuntuacionMeritoAcademicoModelAdmin)
admin.site.register(AsignaturaEspecial, AsignaturaEspecialModelAdmin)
admin.site.register(LicenciaturaGrado, LicenciaturaGradoModelAdmin)
