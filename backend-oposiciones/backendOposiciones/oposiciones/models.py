from django.db import models
from django.utils.text import slugify
from django.db.models.signals import pre_save
from comunidades.models import Comunidad
from especialidades.models import Especialidad

class AsignaturaEspecial(models.Model):
    slug = models.SlugField(primary_key=True, max_length=200, blank=True)
    nombre = models.CharField(unique=True, max_length=200)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = 'Asignaturas Especiales'

class LicenciaturaGrado(models.Model):
    slug = models.SlugField(primary_key=True, max_length=200, blank=True)
    nombre = models.CharField(unique=True, max_length=200)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = 'Licenciaturas y Grados'

class PuntuacionMeritoAcademico(models.Model):
    """
    """
    puntucacion_maxima = models.FloatField()

    # Expediente academico licenciatura
    licenciaturas_grados_requeridos = models.ManyToManyField(LicenciaturaGrado, blank=True, related_name="licenciaturas_grados_requeridos")
    puntucacion_maxima_expediente = models.FloatField(null=True, blank=True)
    puntos_por_asignatura_notable = models.FloatField(null=True, blank=True)
    puntos_por_asignatura_sobresaliente = models.FloatField(null=True, blank=True)
    puntos_por_asignatura_matricula = models.FloatField(null=True, blank=True)
    asignaturas_especiales = models.ManyToManyField(AsignaturaEspecial,  blank=True)

    # Titulos extra
    licenciaturas_grados_relacionados = models.ManyToManyField(LicenciaturaGrado, blank=True, related_name="licenciaturas_grados_relacionados")
    puntuacion_maxima_licenciaturas_grados_relacionados = models.FloatField(null=True, blank=True)
    puntos_por_licenciaturas_grados_extra_sobresaliente = models.FloatField(null=True, blank=True)
    puntos_por_licenciaturas_grados_extra_notable = models.FloatField(null=True, blank=True)
    puntos_por_licenciaturas_grados_extra_aprobado = models.FloatField(null=True, blank=True)

    # Master
    puntuacion_maxima_master = models.FloatField(null=True, blank=True)
    puntos_por_master = models.FloatField(null=True, blank=True)

    # Doctorado
    puntuacion_maxima_doctorado = models.FloatField(null=True, blank=True)
    puntos_por_doctorado = models.FloatField(null=True, blank=True)
    puntos_por_sobresaliente_doctorado = models.FloatField(null=True, blank=True)
    puntos_por_cumlaude_doctorado = models.FloatField(null=True, blank=True)

    # Premios Extraordinarios
    puntuacion_maxima_premio = models.FloatField(null=True, blank=True)
    puntos_por_premio_regional= models.FloatField(null=True, blank=True)
    puntos_por_premio_nacional = models.FloatField(null=True, blank=True)

    # Cursos
    puntuacion_maxima_cursos = models.FloatField(null=True, blank=True)
    requisito_administracion_publica = models.BooleanField(blank=True, null=True)
    requisito_sociedades_cientificas = models.BooleanField(blank=True, null=True)
    requisito_interes_cientifico_curso = models.BooleanField(blank=True, null=True)
    puntos_por_hora = models.FloatField(null=True, blank=True)

    # Ponencias
    puntuacion_maxima_ponencias = models.FloatField(null=True, blank=True)
    requisito_entidad_oficial = models.BooleanField(blank=True, null=True)
    requisito_interes_cientifico = models.BooleanField(blank=True, null=True)
    puntos_por_ponencia = models.FloatField(null=True, blank=True)
    puntos_por_ponencia_primer_firmante = models.FloatField(null=True, blank=True)

    #TODO: Completar publicaciones
    # Publicaciones
    puntuacion_maxima_publicaciones = models.FloatField(null=True, blank=True)
    puntos_por_revista_no_sci = models.FloatField(null=True, blank=True)
    puntos_por_revista_no_sci_segundo_firm = models.FloatField(null=True, blank=True)
    puntos_por_revista_no_sci_primer_firm = models.FloatField(null=True, blank=True)
    puntos_por_revista_sci = models.FloatField(null=True, blank=True)
    puntos_por_revista_sci_segundo_firm = models.FloatField(null=True, blank=True)
    puntos_por_revista_sci_primer_firm = models.FloatField(null=True, blank=True)
    requisito_ambito_nacional = models.BooleanField(blank=True, null=True)
    requisito_ambito_internacional = models.BooleanField(blank=True, null=True)
    puntos_por_capitulo_primer_firm = models.FloatField(null=True, blank=True)
    puntos_por_capitulo = models.FloatField(null=True, blank=True)
    puntos_por_libro_primer_autor = models.FloatField(null=True, blank=True)
    puntos_por_libro = models.FloatField(null=True, blank=True)

    # Otros meritos academicos
    puntuacion_maxima_otros_meritos = models.FloatField(null=True, blank=True)

    class Meta:
        verbose_name_plural = 'Meritos Academicos'

class Oposicion(models.Model):
    """
    """
    slug = models.SlugField(primary_key=True, max_length=221, blank=True)

    especialidad = models.ForeignKey(Especialidad, on_delete=models.CASCADE, null=False)
    comunidad = models.ForeignKey(Comunidad, on_delete=models.CASCADE, null=False)

    meritos_academicos = models.ForeignKey(PuntuacionMeritoAcademico, on_delete=models.DO_NOTHING, null=True)

    def __str__(self):
        return self.slug
    class Meta:
        verbose_name_plural = 'Oposiciones'


def oposicion_pre_save_receiver(sender, instance, *args, **kwargs):
    """
    """
    instance.slug = slugify(instance.comunidad.pk + '-' + instance.especialidad.pk)

def asignatura_especial_pre_save_receiver(sender, instance, *args, **kwargs):
    """
    """
    instance.slug = slugify(instance.nombre)

def licenciatura_grado_pre_save_receiver(sender, instance, *args, **kwargs):
    """
    """
    instance.slug = slugify(instance.nombre)


pre_save.connect(oposicion_pre_save_receiver, sender=Oposicion)
pre_save.connect(asignatura_especial_pre_save_receiver, sender=AsignaturaEspecial)
pre_save.connect(licenciatura_grado_pre_save_receiver, sender=LicenciaturaGrado)
