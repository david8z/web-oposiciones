# Generated by Django 3.1.1 on 2020-09-30 16:48

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('especialidades', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='especialidad',
            options={'verbose_name_plural': 'Especialidades'},
        ),
    ]
