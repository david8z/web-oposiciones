from django.db import models
from django.utils.text import slugify
from django.db.models.signals import pre_save
GRUPOS = (
    ('grupo-a-a1', 'Grupo A (A1)'),
    ('grupo-b-a2', 'Grupo B (A2)'),
    ('grupo-c-c1', 'Grupo C (C1)'),
    ('grupo-d-c2', 'Grupo D (C2)'),
    ('grupo-e', 'Grupo E')
)

ESPECIFICACIONES = (
    ('facultativo-no-sanitario', 'Facultativo no Sanitario'),
    ('facultativo-sanitario', 'Facultativo Sanitario'),
    ('diplomado-no-sanitario', 'Diplomado no Sanitario'),
    ('diplomado-sanitario-especialista', 'Diplomado Sanitario Especialista'),
    ('diplomado-sanitario-no-especialista', 'Diplomado Sanitario no Especialista'),
    ('tecnico-especialista-no-sanitario', 'Técnico Especialista no Sanitario'),
    ('tecnico-especialista-sanitario', 'Técnico Especialista Sanitario'),
    ('tecnico-auxiliar-no-sanitario', 'Técnico Auxiliar no Sanitario'),
    ('tecnico-auxiliar-sanitario', 'Técnico Auxiliar Sanitario'),
    ('personal-de-servicios', 'Personal de Servicios')
)

class Especialidad(models.Model):
    """
    """
    slug = models.SlugField(primary_key=True, max_length=200, blank=True)
    nombre = models.CharField(max_length=200, unique=True)
    grupo = models.CharField(max_length=10, choices=GRUPOS)
    especificacion = models.CharField(max_length=35, choices=ESPECIFICACIONES)

    def __str__(self):
        return self.nombre
    
    class Meta:
        verbose_name_plural = 'Especialidades'


def especialidad_pre_save_receiver(sender, instance, *args, **kwargs):
    """
    """
    instance.slug = slugify(instance.nombre)


pre_save.connect(especialidad_pre_save_receiver, sender=Especialidad)