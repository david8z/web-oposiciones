from django.contrib import admin
from .models import Especialidad
from import_export.admin import ImportExportModelAdmin
from import_export import resources


class EspecialidadResource(resources.ModelResource):
    class Meta:
        model = Especialidad
        import_id_fields = ['slug']

class EspecialidadModelAdmin(ImportExportModelAdmin):
    resource_class = EspecialidadResource

admin.site.register(Especialidad, EspecialidadModelAdmin)
